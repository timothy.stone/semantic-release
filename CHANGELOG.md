## [3.5.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.5.0...3.5.1) (2023-09-19)


### Bug Fixes

* always install extra plugins ([f011acd](https://gitlab.com/to-be-continuous/semantic-release/commit/f011acd9b92d67cb73ecc4612d9afb921f5258fb)), closes [#29](https://gitlab.com/to-be-continuous/semantic-release/issues/29)

# [3.5.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.2...3.5.0) (2023-08-04)


### Features

* make commit message configurable ([fbc63aa](https://gitlab.com/to-be-continuous/semantic-release/commit/fbc63aa9ed00c6714157cef53e3173433dd641a9))

## [3.4.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.1...3.4.2) (2023-07-13)


### Bug Fixes

* activate debug when TRACE is set ([fc3668b](https://gitlab.com/to-be-continuous/semantic-release/commit/fc3668b63b562e607418ba0023e5df34450dcfc5))

## [3.4.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.4.0...3.4.1) (2023-07-13)


### Bug Fixes

* make install_yq quiet ([61b7413](https://gitlab.com/to-be-continuous/semantic-release/commit/61b74134ce39b9e438f3999d22bc0db7f4f018bb))

# [3.4.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.3.1...3.4.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([40c252e](https://gitlab.com/to-be-continuous/semantic-release/commit/40c252e62fd954370d654572d753294f28122935))

## [3.3.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.3.0...3.3.1) (2023-03-27)


### Bug Fixes

* fix a regression in semantic-release-info job ([1b79f5d](https://gitlab.com/to-be-continuous/semantic-release/commit/1b79f5dfe6c2ae4ba29e73a18d6bcd5c09688da8))

# [3.3.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.2...3.3.0) (2023-03-21)


### Features

* allow to specify sem-rel and plugins version ([6b38d5b](https://gitlab.com/to-be-continuous/semantic-release/commit/6b38d5bc48157b8c6aef8f847a57b75faaf693dc))

## [3.2.2](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.1...3.2.2) (2023-02-27)


### Bug Fixes

* mark current directory as safe for git ([8f7e0be](https://gitlab.com/to-be-continuous/semantic-release/commit/8f7e0be5bbd7870515f7144d37b6ad4f9201ee7e))

## [3.2.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.2.0...3.2.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([81c4144](https://gitlab.com/to-be-continuous/semantic-release/commit/81c4144b2acfebff812c8e75a4fa430316977ce8))

# [3.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.1.1...3.2.0) (2022-12-05)


### Features

* display computed semantic-release-info ([5a1a37b](https://gitlab.com/to-be-continuous/semantic-release/commit/5a1a37be5df577d249205b917072467af208c392))

## [3.1.1](https://gitlab.com/to-be-continuous/semantic-release/compare/3.1.0...3.1.1) (2022-11-26)


### Bug Fixes

* support custom CA certificates with npm ([bf79a7d](https://gitlab.com/to-be-continuous/semantic-release/commit/bf79a7df66f4585288f872477eb386294a372b12))

# [3.1.0](https://gitlab.com/to-be-continuous/semantic-release/compare/3.0.0...3.1.0) (2022-10-16)


### Features

* enable commit signing ([dd6129b](https://gitlab.com/to-be-continuous/semantic-release/commit/dd6129b6e5dc9c9fad269add6a22d49ab003f5d0))

# [3.0.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.3.1...3.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([6128ce6](https://gitlab.com/to-be-continuous/semantic-release/commit/6128ce69cb57f407db307a2b775e31dca1c1a403))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

## [2.3.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.3.0...2.3.1) (2022-06-07)


### Bug Fixes

* install yq only if not found in image ([23041f1](https://gitlab.com/to-be-continuous/semantic-release/commit/23041f1a463079954184b831b5b40bbef8530ddc))

# [2.3.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.6...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([b18fd89](https://gitlab.com/to-be-continuous/semantic-release/commit/b18fd89e223f84723f44c04d5b364bb375d78f16))

## [2.2.6](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.5...2.2.6) (2022-04-08)


### Bug Fixes

* remove verifyconditions in semrel-info ([e2e7b8f](https://gitlab.com/to-be-continuous/semantic-release/commit/e2e7b8f9bb094f172351dc6886f82efe718e115b))

## [2.2.5](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.4...2.2.5) (2022-03-19)


### Bug Fixes

* generate .releaserc in yaml format ([6e377bd](https://gitlab.com/to-be-continuous/semantic-release/commit/6e377bda26b7e5e2ae10d3b4b4c7b51e74640896))

## [2.2.4](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/semantic-release/compare/2.2.3...2.2.4) (2022-03-15)


### Bug Fixes

* don't overwrite existing .releaserc conf in semrel-info ([11c64dd](https://git-us-east1-c.ci-gateway.int.gprd.gitlab.net:8989/to-be-continuous/semantic-release/commit/11c64ddc53f65c77067bcf97649c8e23d948b05a))

## [2.2.3](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.2...2.2.3) (2022-02-25)


### Bug Fixes

* semantic-release is manual non-blocking by default ([fc8efc8](https://gitlab.com/to-be-continuous/semantic-release/commit/fc8efc85cbf86686cad40100c6377880152ccb12))

## [2.2.2](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.1...2.2.2) (2022-02-14)


### Bug Fixes

* add main branch for configuration generation  ([eb82e43](https://gitlab.com/to-be-continuous/semantic-release/commit/eb82e43dde2a3fdc23c83eeb33071a88f9f2560b)), closes [#14](https://gitlab.com/to-be-continuous/semantic-release/issues/14)

## [2.2.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.2.0...2.2.1) (2022-01-08)


### Bug Fixes

* set semantic-release job to download no artifacts at all ([ceb1379](https://gitlab.com/to-be-continuous/semantic-release/commit/ceb137958da6acceb368b1f1b4ba56dae0c4eda1))

# [2.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.1.0...2.2.0) (2021-11-23)


### Features

* overridable semrel config dir ([6bd0e4d](https://gitlab.com/to-be-continuous/semantic-release/commit/6bd0e4df9190ed75689e966d90946b35d7fd44f5))

# [2.1.0](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.5...2.1.0) (2021-11-16)


### Features

* allow to install additional npm package ([561c332](https://gitlab.com/to-be-continuous/semantic-release/commit/561c332331f02ef965fb32b52556a182e90c7b0e))

## [2.0.5](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.4...2.0.5) (2021-11-15)


### Bug Fixes

* prevent loops ([4ec78ea](https://gitlab.com/to-be-continuous/semantic-release/commit/4ec78ead772e7b4f88bcc3fe22f3bf8e561b823c))

## [2.0.4](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.3...2.0.4) (2021-10-07)


### Bug Fixes

* use master or main for production env ([56d8527](https://gitlab.com/to-be-continuous/semantic-release/commit/56d8527d8be27f4e1335f64fd29e5697abe1da9d))

## [2.0.3](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.2...2.0.3) (2021-09-27)


### Bug Fixes

* handle absent release property in package.json ([a5c73a9](https://gitlab.com/to-be-continuous/semantic-release/commit/a5c73a9d51d35ff98768d336467574c7b260ba4b))

## [2.0.2](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.1...2.0.2) (2021-09-09)

### Bug Fixes

* add changelog ([1682e12](https://gitlab.com/to-be-continuous/semantic-release/commit/1682e1274ad2524ac6b0805f19799cd824ef7806))

## [2.0.1](https://gitlab.com/to-be-continuous/semantic-release/compare/2.0.0...2.0.1) (2021-09-09)

### Bug Fixes

* make plugins installation faster ([bab645e](https://gitlab.com/to-be-continuous/semantic-release/commit/bab645e0a731aa5318dd2569868eb4886bbde50b))

## [2.0.0](https://gitlab.com/to-be-continuous/semantic-release/compare/1.2.0...2.0.0) (2021-09-01)


* Merge branch '3-change-variable-behaviour' into 'master' ([97daa97](https://gitlab.com/to-be-continuous/semantic-release/commit/97daa97bbbf5f956f176474b293a41eae4c6031d)), closes [#3](https://gitlab.com/to-be-continuous/semantic-release/issues/3)


### Features

* Change boolean variable behaviour ([077e9c0](https://gitlab.com/to-be-continuous/semantic-release/commit/077e9c0a1f75ffeece8f57f9ae970863cca08744))


### BREAKING CHANGES

* Change boolean variable behaviour
* boolean variable now triggered on explicit 'true' value


## [1.2.0](https://gitlab.com/to-be-continuous/semantic-release/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([9b39127](https://gitlab.com/to-be-continuous/semantic-release/commit/9b391279128aa00d1ec1b6dc09fe8a6f82168bba))


## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/compare/1.0.1...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([fe5e9d6](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/fe5e9d68e380d6b41f0cffa95e1bff2bb77dc42e))


## [1.0.1](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/compare/1.0.0...1.0.1) (2021-05-12)

### Bug Fixes

* **generated-releaserc:** use strict yaml to avoid unexpected syntax error ([e68c72b](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/e68c72b070b4a91be0e217863a077ec2e4450f04))


## 1.0.0 (2021-05-06)


### Features

* initial release ([c711a35](https://gitlab.com/Orange-OpenSource/tbc/semantic-release/commit/c711a35b9f634372c420ab8fb5bb5cfaf87d384b))
